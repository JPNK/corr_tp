# Introduction

Auteur : {{ book.author }}

Date de fabrication : {{ gitbook.time }}

Point de départ : [https://gitlab.com/pages/gitbook](https://gitlab.com/pages/gitbook)

## Ebooks

* [Support en formation PDF](/corr_tp.pdf)
* [Support en formation EPUB](/corr_tp.epub)
* [Support en formation MOBI](/corr_tp.mobi)

![Superman S symbol](https://upload.wikimedia.org/wikipedia/commons/0/05/Superman_S_symbol.svg)

Source de l'image : [Superman S symbol](https://commons.wikimedia.org/wiki/File:Superman_S_symbol.svg)
